﻿using System;

namespace BinarySearchTask
{
    public static class ArrayExtension
    {
        public static int? BinarySearch(int[] source, int value)
        {
            if (source is null)
            {
                throw new ArgumentNullException(nameof(source));
            }

            int lastIndex = source.Length - 1;
            int firstIndex = 0;

            while (firstIndex <= lastIndex)
            {
                int midIndex = (firstIndex + lastIndex) / 2;

                if (value < source[midIndex])
                {
                    lastIndex = midIndex - 1;
                    continue;
                }

                if (value > source[midIndex])
                {
                    firstIndex = midIndex + 1;
                    continue;
                }

                return midIndex;
            }

            return null;
        }
    }
}
